from django.db import models


class Student(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    major = models.CharField(max_length=50)
    year = models.CharField(max_length=50)
    gpa = models.DecimalField(max_digits=3, decimal_places=2)

    def __str__(self): 
    	return self.title

class Course(models.Model):
    course_id = models.IntegerField()
    course_title = models.CharField(max_length=200)
    course_name = models.CharField(max_length=200)
    course_section = models.IntegerField()
    course_department = models.CharField(max_length=200)
    instructor_full_name = models.CharField(max_length=200)
			 		

# Create your models here.
