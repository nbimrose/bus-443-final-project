from django.shortcuts import render
from django.http import HttpResponse
from .models import Student, Course
from django.db import connection 
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.views.generic.list import ListView
from django.db import models
from django.db.models import Avg
from django.core.serializers.json import DjangoJSONEncoder
import json



# Create your views here.
class About(TemplateView):
	template_name = 'StudentInfo/about.html'
#@login_required	
def about(request):
	return render(request, 'StudentInfo/about.html')

@login_required
def Gradrates(request):
	cursor = connection.cursor()
	cursor.execute("SELECT * FROM \"StudentInfo_Rates\"")
	result = dictfetchall(cursor)

	gradYear = []
	fourYear = []
	fiveYear = []
	sixYear = []

	for rate in result:
		gradYear.append(rate['gradyear'])
		fourYear.append(rate['4YrGradRate'])
		fiveYear.append(rate['5YrGradRate'])
		sixYear.append(rate['6YrGradRate'])
	cursor.close()
	
	return render(request, 'studentinfo/gradrate.html', 
		{
			'gradYear': json.dumps(gradYear) , 
			'fourYear': json.dumps(fourYear, cls=DjangoJSONEncoder) , 
			'fiveYear': json.dumps(fiveYear, cls=DjangoJSONEncoder) , 
			'sixYear': json.dumps(sixYear, cls=DjangoJSONEncoder)
		}
	)


@login_required
def home(request):
	CountStudents = Student.objects.all().count()
	CourseCount = Course.objects.all().count()
	AvgGpa = list(Student.objects.aggregate(Avg('gpa')).values())[0]
	AvgGpa = round(AvgGpa,3)
	cursor = connection.cursor()
	cursor.execute("SELECT COUNT(DISTINCT student_id) FROM \"StudentInfo_enrollment\"")
	EnrollCount = cursor.fetchone()
	cursor.close()
	return render(request, 'studentinfo/index.html', {'CountStudents': CountStudents , 'CourseCount': CourseCount, 'AvgGpa': AvgGpa, 'EnrollCount': EnrollCount[0]})


def dictfetchall(cursor):
	columns = [col[0] for col in cursor.description]
	return[ dict(zip(columns,row)) for row in cursor.fetchall()]
	


class Studentdetails(LoginRequiredMixin, ListView):
	template_name = 'studentinfo/student.html'
	queryset = Student.objects.all()
	context_object_name= 'student'
	paginate_by = 10

class Courses(LoginRequiredMixin, ListView):
	template_name = 'studentinfo/course.html'
	queryset = Course.objects.all()
	context_object_name= 'course'
	paginate_by = 10

@csrf_exempt
def Handleenrollment(request, studentId=0):
	if request.method == 'GET':
		return Getenrollmentinfo(request, studentId)
	elif request.method == 'POST':
		return Setenrollmentinfo(request, studentId)

def Getenrollmentinfo(request, studentId=0):
	cursor = connection.cursor()
	cursor.execute("SELECT * FROM \"StudentInfo_enrollment\" WHERE student_id={}".format(studentId))
	enrollment = dictfetchall(cursor)
	cursor.close()
	return HttpResponse(json.dumps(enrollment, cls=DjangoJSONEncoder), content_type="application/json")

def Setenrollmentinfo(request, studentId=0):
	courses_list = json.loads(request.body)

	if len(courses_list) > 3:
		return HttpResponse(json.dumps({ "status": "error", "message": "Enrollment limited to 3 courses. Please select fewer courses." }), content_type="application/json")
	cursor = connection.cursor()
	cursor.execute("DELETE FROM \"StudentInfo_enrollment\" WHERE student_id={}".format(studentId))
	query = ""
	index = 0
	for course in courses_list:
		query = query + "({}, {})".format(studentId, course)
		if index < (len(courses_list) - 1):
			query += ','
		index += 1
	try:
		cursor.execute("INSERT INTO \"StudentInfo_enrollment\" (student_id, course_id) VALUES {}".format(query))
	except Exception:
		return HttpResponse(json.dumps({ "status": "error", "message": "Enrollment failed" }))
	cursor.close()
	return HttpResponse(json.dumps({ "status": "success", "message": "Enrollment successful" }))


@login_required
def Enrollmentinfo(request):
	course = Course.objects.all()
	student = Student.objects.all()
	return render(request, 'StudentInfo/enrollment.html', {'course':course, 'student' : student})

	