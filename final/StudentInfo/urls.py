from django.urls import path, include, reverse
from django.contrib import admin
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('about/', views.About.as_view(), name="about"),
	path('student/', views.Studentdetails.as_view(), name="student"),
    path('course/', views.Courses.as_view(), name="course"),
	path('enrollment/', views.Enrollmentinfo, name="studentinfo"),
    path('gradrate/', views.Gradrates, name="gradrate"),
    path('enrollment/student/<str:studentId>', views.Handleenrollment)
]
